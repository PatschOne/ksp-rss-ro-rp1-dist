**KSP-RSS-RO-RP1-Dist**

You will have to make a tough decision when it comes to **_30-Life Support_** later which may end up having another two lesser though decisions :D. 
If you decide to go for Option A) you should consider to not use KerbalKonstructionTime (first lesser though decision) and TestFlight (second lesser though decision) since your progress will slow down drastically!
And if you like the way science work in Kerbalism but you don't want the other stuff, there is the "Kerbalism Science Only" mod in LifeSupportAddidtional.
Anyway, adjusting Kerbalism to your needs is perfectly fine. No one kills you if you lower Radiation settings or Part Failure settings and so on.

**The most important thing is that you're enjoying play the game - thats the only thing that matters here!**

  I. PREFACE
===============================================================================
 
- download the mod files to the respective "file" folder like the links are in
- don't copy ModuleManager dlls
- _[Optional]_ copy Ship folders one level above the GameData folder of the mod's zip if there are Ship folders :-P
- take care, some mods have another folder level before the mods GameData folder itself, so always check for its existence within the mods zip! Some mods don't have a GameFolder at all - so you're facing the mod itself.
- only install _folders_ within the mods zip, no extra files until it is explicitely written
- only install the main mod folder below GameData and not the packed other mods around, also don't dive deeper down the folder structure as one level below GameData until it is explicitely written, i.e.
	* for FAR_0_15_11_2_Mach.zip, only extract FerramAerospaceResearch folder to your GameData folder
	* for RasterPropMonitor install JSI folder, don't dive into JSI and only extract RasterPropMonitor - confusing, eh?

- ignore RSSVE incompatibility warning
- ignore the KSP-AVC information about 2.1.1.3 KSPAPIExtensions since it is a pre release!

- "Blacklisted" Mods, see **_99-CurrentlyDisabled_** for reasons
	* BetterBurnTime 1.9.1
	* TweakScale 2.4.3.8 (latest for < 1.8)
	* ROSolar
	* Waypointmanager

 II. INSTALLATION
===============================================================================
Normally you can go straight thru any point until you reach point 50 while ignoring any "\*Additional" mods and you will end up with a quite good working installation. Just add some QOL, parts and sounds and visual mods. Done.
If you go thru until the end, also ignoring the "\*Additional" mods you'll have a quite comprehensive but still playable installation working on an up-to-date computer with 16+GB RAM and 4+GB Video RAM.
With each mod you install aside the "main path" will increase CPU / GPU load and RAM usage. You'll need NASAs Supercomputer to play the game then, so take care! The cruel thing is, you'll notice that not from the beginning, but after a while when you progress and your vessels get bigger, it starts stuttering in VAB, than inflight and then everywhere.

**DX11 Mode / KS3P / Texture Unlimited**
At the point you're using Textures Unlimited you should start KSP with _-force-d3d11_ option which lowers RAM usage for main and video memory. Without Texture Unlimited and that d3d11 option you may end up in blue colored parts in VAB / SPH.
I have no other solution for that as using Textures Unlimited and the d3d11 option. KS3P fitts also perfectly to the d3d11 option!

Ok! 'Nuff said! Let's start!

**0. Base / Minimal Installation**
The _00-Base-RSS-RO-RP1_ installation is the very basic RSS-RO-RP1 installation. It contains hard dependencies only.

**_00-Base-RSS-RO-RP1_**
- for RealSolarSystem_Textures_xxxx.zip.url select ONE!
- INFO: the NoNon* folders removing the non RO and Non RP0 parts from the VAB/SPH parts menu
- RealismOverhaul
	* install EngineGroupController too
- VenStockRevamp
	* don't miss the instructions in 50_Parts::Restock if you gonna use Restock
- SXT
	* You will need to open the newly-installed SXT folder in after installation and double-click on the file Windows_CopyTextures.bat. If you're not on Windows, there should be a bash script. If you're unsure of what (or why) you're doing, read the README in that Github repo.
- FireSpitter
	* there is an additional Folder "For Release" before the Firespitter mod folder, so you have to dive one deeper ;-)
	* delete everything in the Mod directory except Plugins and Resources!
- Module Manager
	* goes directly to the GameData folder
	* Overwrite if asked

**_00-Minimal_**

- KSPAPIExtensions
	* install the 000_KSPAPIExtensions folder and the 000_KSPe.dll
	
**_01-MinimalPatches_**

- nothing special	
-------------------------------------------------------------------------------
**ATTENTION:**  In case you stop here with your installation
-= REWORK TO BE DONE =-
- remove all ModuleManager.dlls except 4.0.3
- you may now install Gfx, Sound or QOL - Mods of 90-xx Folders -> check their 98-FinalPatches
-------------------------------------------------------------------------------

**1. Recommended mods**

**_10-PreRecommended_**

- nothing special
	
**_11-Recommended_**

- ProceduralFairings 1.6.2 contains wrong Texture references in config. The easiest and laziest workaround is to copy the whole Assets folder under ProceduralFairings into ProceduralFairings/Parts/. Do a real Copy, do not just moving it!

-------------------------------------------------------------------------------
**ATTENTION:**  In case you stop here with your installation

- delete the respective folder in your GameData, overwrite with the contents of the 33-PartsPatches folder:
	* CommunityResourcePack (CRP)
	* CommunityTechTree
- remove all ModuleManager.dlls except 4.0.3
- you may now install Gfx, Sound or QOL - Mods of 90-xx Folders -> check their 98-FinalPatches
-------------------------------------------------------------------------------

**2. Suggested mods**
	
**_20-Suggestions_**

- I just wanna remind you to not copy any other folder beneath the main mod folder! ;-) (KIS / KAS - you'll see)
- RSSVE
	* requires EnvironmentalVisualEnhancements to be installed
	* just copy the RSSVE folder below the RSSVE-xxxxx folder in the zip
	* in RSSVE -> Texture -> MainTextures remove the _LR/_HR extension from file names depending if you wanna have Low Res or High Res textures to be installed
	if only one equivalent is available, remove that respective extension either
	
-------------------------------------------------------------------------------
**ATTENTION:**  In case you stop here with your installation

- remove all ModuleManager.dlls except 4.0.3
- you may now install Gfx, Sound or QOL - Mods of 90-xx Folders -> check their 98-FinalPatches
-------------------------------------------------------------------------------

**3. Life Support**

- here you have to make a hard decision:
	* A) Kerbalism
	
	OR
	
	* B) TAC

**_30-LifeSupport_**

A) uses Real Antennas, nice continous science income but completely different approach as you experienced so far (you should watch some videos first!).

**_DON'T INSTALL CONNECTED LIVINGSPACE WHEN USING KERBALISM DUE DO INCOMPATIBILITY_**

- ROKerbalism
	* only install KerbalismConfig

B) TAC works best with RemoteTech, but RemoteTech is known for producing lags. You may use RealAntennas as a replacement for RemoteTech here either, but its not recommended in a non Kerbalism environment since it can highly happen, that science will not be transmitted and you have to fiddle around with the RealAntenna setting to get it work! And even then it's not sure if it will work in any case!

- TAC-Package
	* TacLifeSupport: contains another folder level, take care
	* KerbalHealth
	* RemoteTech
	* ScienceAlert (NOT NEEDED/WORKING in Kerbalism due to own ScienceModule)
	* \[ x\] Science (NOT NEEDED/WORKING in Kerbalism due to own ScienceModule)

- TacLifeSupport
	* install REPOSoftTech and ThunderAerospace
- x.Science
	* contains no GameData folder, just copy the one and only folder in the zip

**4. Mods that changes the game mechanic**

**_40-GameMechanic_**

- ConnectedLivingSpace
    * **_DON'T INSTALL CONNECTED LIVINGSPACE WHEN USING KERBALISM DUE DO INCOMPATIBILITY_**

**_41-GameMechanicAdditional_**

- this is the first "Additional" folder. Mods in here will increase the immersion a lot but also 
increase the "waiting" time and possible part failures immense. Even that **KerbalConstructionTime** and **TestFlightCore** tagged by RO/RP1 as dependency you may skip them. Im running the installation fine without them. There are some log messages which telling you, that the mods are missing. Its up to you. But also beeing said, **Kerbalism adds part failures too**. Decide for your own!
- EngineIgnitor
	* Engines have limited number of ignition, this mod adds ignitor as resource
- Principia
	* adds n-body physics
	
-------------------------------------------------------------------------------
**ATTENTION:**  In case you stop here with your installation

- remove all ModuleManager.dlls except 4.0.3
- you may now install Gfx, Sound or QOL - Mods of 90-xx Folders -> check their 98-FinalPatches
-------------------------------------------------------------------------------

**5. All the part mods**

**_50-Parts_**

- Coatl-Aerospace
	* got another folder bevore the GameData - take care
- Konstruction
	* copy 000_USITools and UmbraSpaceIndustries
- RealISRU
	* got another folder bevore the GameData - take care
- Restock
	* You should already have VenStockRevamp 1.13.0; delete GameData/VenStockRevamp/PathPatches and GameData/VenStockRevamp/Squad/data
	
**_52-PartsAdditional_**

- things here mainly increases RAM usage, high(er) CPU load and stuttered building in (VAB) starting around the 1970 tech when you build up bigger vessels. Copy only single mods from there only if you can not life without them. Its for your own saftey :-P

- KSPIE
    * install it first
    * copy only InterstellarHybridRocketry and WarpPlugin
    * then copy InterstellarFuelSwitch out of its standalone mod
    * then copy PhotonSail out of its standalone mod
    * install tweakscale out of its standalone mod including the dll
    * make sure that there is no other _scale_redist.dll_ in any other mod
 
- BDanim and CritterCrawlerRetractingVectoringEngines belong together
	
-------------------------------------------------------------------------------
**ATTENTION:**  In case you stop here with your installation

- remove all ModuleManager.dlls except 4.0.3
- you may now install Gfx, Sound or QOL - Mods of 90-xx Folders -> check their 98-FinalPatches
-------------------------------------------------------------------------------
	
**9. Quality Of Life and Visual&Sound mods**

Yes, you'll need them... ALL! No you don't, just kidding ;-)
But its really hard to decide which one makes it into the install and which not. So don't take it too serious,
also taking one of the "\*Additional" isn't bad ;-)

**_90-QOL_**

Again, only install the main mod folder if nothing other is mentioned.

- Craftmanager: dont forget its dependency KXAPI
- KSPAlternateResourcePanel_2.9.3.0.zip: dont delete TriggerTech folder, copy and overwrite
- S.A.V.E: dont delete Nereid folder, copy and overwrite
	* in my optinion you should defenitely use it even if you play "hardcore" (no quicksave and reload) since some times KSP differs from the real World by chrashing completely.
	In RSS things will take quite more time than in standard KSP so you may loose a lot progress by crashes. But you have to use it honestly.
- CTI (Community Trait Icons) is a base library provide stuff for other mod, it doesn't make something by it self
	* here we'll use it for Portrait_Stats (there a other which are beautiful, just search for CTI) but always remember: each additional mod is a source of errors and load!
- KerbalAlarmClock and KSPAlternateResourcePanel share the main folder TriggerTech, don't delete this folder before, just overwrite if asked
	* if you want, you may install the Flags folder as well
- FinalFrontier and S.A.V.E share the main folder Nereid, don't delete this folder before, just overwrite if asked

**_91-QOLAdditional_**
- CapCom and Contracts_Windows share the main folder DMagicUtilities, don't delete this folder before, just overwrite if asked

**_95-Visuals&Sound_**
- Chatterer-0.4
	* there is another subfolder before you come to the GameData folder - take care
	* copy and overwrite existing stuff
- Real Chatterer
	* dont delete the Chatterer folder in GameData, copy and overwrite existing stuff
- DistantObject
	* copy the main Mod folder under GameData folder
	* beneath the GameData folder you'll find Alternate Planet Color Configs, dive into it and copy the DistantObject folder under Real Solar System (default) folder to your GameData folder, overwrite if asked
- EngineLight
	* yes, its not the EngineLightRelit. Since it is known for Log-Spamming and some lagging
- RocketSoundEnhancement
	* there is another subfolder before you come to the GameData folder - take care
- AmbientLightAdjustment
	* decide if you using this or MinimumAmbientLight or the more famous PlanetShine, I'm prefering this
	* ingame, if you're using scatterer, bring up its UI and remove the selection of "Disable scaled space ambient light"
	* see my little video about: https://www.youtube.com/watch?v=uPDe7ZbdVAQ&feature=youtu.be

**_96-Visuals&SoundAdditional_**
- FlareReplacer
	* just download the flare of your taste - or not
- BasicProceduralTexture: make sure you dive down to the GameData folder in the zip and copy its content

-------------------------------------------------------------------------------
**ATTENTION:** When you install KS3P and/or ReStock(+) add -force-d3d11 to the KSP-Start

- check for 98-FinalPatches
-------------------------------------------------------------------------------
	
**_98-FinalPatches_**

- **MANDATORY!** Copy folder by folder if you've installed the respective mod, don't delete the folder in GameData, just overwrite
	
**_99-CurrentlyDisabled_**
- BetterBurnTime 1.9.1 due to NRE in latest 1.7.3 code when you use MechJeb - which will never be fixed and causes BBT to not calculate the BurnTime correctly - so its kinda useless, when you use MechJeb. If not, install it :D
- TweakScale 2.4.3.8 (latest for < 1.8) due to an endless list of issues https://github.com/net-lisias-ksp/TweakScale/blob/master/KNOWN_ISSUES.md but if you gonna install PartsAdditional you'll have no choice since it is a hard requirement for InterstellarFuelSwitch and Interstellar Extended mods as well as for PhotonSail
- ROSolar is a brand new mod in v0.1 which contains only the parts yet but without any attributes! Dont Use it!
- Waypointmanager has no RSS / RO config
	
III. Setup your new Save in the KSC
===============================================================================	
- KRASH
	* open it and select the RP-0 config on the left and accept the selection
- scatterer
	* disable "Disable scaled space ambient light" 
- distant object enhancement enable
	* show names on mouse over
	* distant vessel rendering
- Ambient mod
	* in the toolbar mod (the one with de colored squares floating around)
- E.V.E
	* goto Tracking Station, there will be a lot of stuttering when you open this the first time and it will take a while to calm down (10 seks)
	* open E.V.E UI, coto CitylightsManager-Tab and press Apply (unfortunately you always hate to do it if you're loding the game or even a save game)

IV. Thing you should know
===============================================================================

 - solarpanels are heavily nerfed by RO & RP-1
	* documentation will follow - or maybe not :-P
 	
 V. Known issues
===============================================================================

T.B.D.